FROM rust:latest
VOLUME /app/build
WORKDIR /app/build
ENTRYPOINT ["cargo", "build"]
CMD ["--release", "--target", "x86_64-unknown-linux-musl"]
RUN apt-get update && apt-get -y install musl-tools
RUN rustup target add x86_64-unknown-linux-musl
