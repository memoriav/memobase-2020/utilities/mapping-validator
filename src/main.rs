#![allow(dead_code)]

use serde::Deserialize;
use std::env;
use std::fs::File;
use std::process;

#[derive(Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
struct Mapping {
    record: Record,
    physical: Option<PhysicalObject>,
    digital: Option<DigitalObject>,
    thumbnail: Option<ThumbnailObject>,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
struct Record {
    uri: DirectMapField,
    #[serde(rename = "type")]
    _type: DirectMapOrRecordTypeConstantField,
    name: Option<Any>,
    title: Option<Any>,
    scope_and_content: Option<Any>,
    same_as: Option<Any>,
    #[serde(rename = "abstract")]
    _abstract: Option<Any>,
    source: Option<Any>,
    descriptive_note: Option<Any>,
    relation: Option<Any>,
    conditions_of_use: Option<Any>,
    conditions_of_access: Option<Any>,
    is_sponsored_by_memoriav: Option<bool>,
    rights: Option<Rights>,
    titles: Option<Vec<Title>>,
    identifiers: Option<Identifier>,
    languages: Option<Languages>,
    subject: Option<Vec<Concept>>,
    genre: Option<Vec<Concept>>,
    place_of_capture: Option<Place>,
    related_places: Option<Vec<Place>>,
    creation_date: Option<DirectMapField>,
    issued_date: Option<DirectMapField>,
    temporal: Option<DirectMapField>,
    creators: Option<Vec<AgentWithRelationName>>,
    contributors: Option<Vec<AgentWithRelationName>>,
    producers: Option<Vec<Agent>>,
    related_agents: Option<Vec<Agent>>,
    published_by: Option<Vec<Agent>>,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
struct PhysicalObject {
    descriptive_note: Option<Any>,
    duration: Option<Any>,
    physical_characteristics: Option<Any>,
    colour: Option<Any>,
    conditions_of_use: Option<Any>,
    conditions_of_access: Option<Any>,
    rights: Option<Rights>,
    identifiers: Option<Identifier>,
    carrier_type: Option<Any>,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
struct DigitalObject {
    descriptive_note: Option<Any>,
    locator: Option<Any>,
    duration: Option<Any>,
    conditions_of_use: Option<Any>,
    conditions_of_access: Option<Any>,
    proxy: Option<DirectMapOrProxyConstantField>,
    identifiers: Option<Identifier>,
    rights: Option<Rights>,
}

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
struct ThumbnailObject {
    locator: Any,
}

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
struct Rights {
    holder: Option<Any>,
    access: Option<Any>,
    usage: Option<UsageRights>,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
struct UsageRights {
    name: Any,
    same_as: Any,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
struct Identifier {
    call_number: Option<PrefixOrDirectMapField>,
    old_memobase: Option<PrefixOrDirectMapField>,
    original: Option<DirectMapField>,
}

#[derive(Deserialize)]
#[serde(untagged)]
enum Title {
    Main { main: Any },
    Series { series: Any },
    Broadcast { broadcast: Any },
}

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
struct Languages {
    content: Option<Any>,
    caption: Option<Any>,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
struct Concept {
    pref_label: Option<Any>,
    alt_label: Option<Any>,
    hidden_label: Option<Any>,
    change_note: Option<Any>,
    definition: Option<Any>,
    editorial_note: Option<Any>,
    example: Option<Any>,
    history_note: Option<Any>,
    notation: Option<Any>,
    note: Option<Any>,
    scope_note: Option<Any>,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
struct Place {
    name: Any,
    same_as: Option<Any>,
    coordinates: Option<Any>,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
enum AgentWithRelationName {
    Agent(AgentWithRelationNameBase),
    CorporateBody(AgentWithRelationNameBase),
    Person(AgentWithRelationNameExtended),
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
enum Agent {
    Agent(AgentBase),
    CorporateBody(AgentBase),
    Person(AgentExtended),
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
struct AgentBase {
    name: Any,
    same_as: Option<Any>,
    has_variant_name_of_agent: Option<Any>,
    history: Option<Any>,
    descriptive_note: Option<Any>,
    has_period_of_activity_of_agent: Option<Any>,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct AgentWithRelationNameBase {
    #[serde(flatten)]
    base: AgentBase,
    relation_name: Option<Any>,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
struct AgentExtended {
    #[serde(flatten)]
    base: AgentBase,
    gender: Option<Any>,
    has_profession_or_occupation: Option<Any>,
    is_member_of: Option<Any>,
    has_birth_date: Option<Any>,
    has_death_date: Option<Any>,
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
struct AgentWithRelationNameExtended {
    #[serde(flatten)]
    base: AgentBase,
    relation_name: Option<Any>,
    gender: Option<Any>,
    has_profession_or_occupation: Option<Any>,
    is_member_of: Option<Any>,
    has_birth_date: Option<Any>,
    has_death_date: Option<Any>,
}

#[derive(Deserialize)]
pub struct DirectMapField(String);

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ConstantField {
    #[serde(rename = "const")]
    _const: String,
}

#[derive(Deserialize)]
#[serde(untagged)]
pub enum DirectMapOrRecordTypeConstantField {
    RecordTypeConstantField(RecordTypeConstantField),
    DirectMapField(DirectMapField),
}

#[derive(Deserialize)]
#[serde(untagged)]
pub enum PrefixOrDirectMapField {
    PrefixField(PrefixField),
    DirectMapField(DirectMapField),
}

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
pub struct RecordTypeConstantField {
    #[serde(rename = "const")]
    _const: RecordType,
}

#[derive(Deserialize)]
enum RecordType {
    Film,
    Foto,
    Radio,
    Ton,
    Tonbildschau,
    TV,
    Video,
}

#[derive(Deserialize)]
#[serde(untagged)]
pub enum DirectMapOrProxyConstantField {
    ProxyConstantField(ProxyConstantField),
    DirectMapField(DirectMapField),
}

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ProxyConstantField {
    #[serde(rename = "const")]
    _const: ProxyConstant,
}

#[derive(Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum ProxyConstant {
    File,
    Proxy,
    Proxydirect,
    Redirect,
}

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
pub struct PrefixField {
    prefix: Prefix,
}

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Prefix {
    value: String,
    field: String,
}

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
pub struct LanguageField {
    de: Option<MixedContent>,
    fr: Option<MixedContent>,
    it: Option<MixedContent>,
}

#[derive(Deserialize)]
#[serde(untagged)]
pub enum MixedContent {
    ConstantField(ConstantField),
    DirectMapField(DirectMapField),
    PrefixField(PrefixField),
    ListField(ListField),
}

#[derive(Deserialize)]
pub struct ListField(Vec<ListElement>);

#[derive(Deserialize)]
#[serde(untagged)]
pub enum ListElement {
    ConstantField(ConstantField),
    DirectMapField(DirectMapField),
    PrefixField(PrefixField),
}

#[derive(Deserialize)]
#[serde(untagged)]
pub enum Any {
    ConstantField(ConstantField),
    DirectMapField(DirectMapField),
    PrefixField(PrefixField),
    ListField(ListField),
    LanguageField(LanguageField),
}

// pub fn deprecation_warning<'de, D>(deserializer: D) -> Result<Option<Any>, D::Error>
// where
//     D: Deserializer<'de>,
// {
//     println!("Deprecated!");
//     Any::deserialize(deserializer)
// }

fn main() {
    let mut exit_code = 0;
    if env::args().len() <= 1 {
        println!("☠ No files to validate");
        exit_code = 1;
        process::exit(exit_code);
    }
    for file_path in env::args().skip(1) {
        let file = if let Ok(f) = File::open(&file_path) {
            f
        } else {
            println!("☠ {} can't be opened", &file_path);
            exit_code = 1;
            continue;
        };
        let maybe_yaml: Result<Mapping, serde_yaml::Error> = serde_yaml::from_reader(file);
        if let Err(e) = maybe_yaml {
            println!("⚠ {} has errors: {}", &file_path, e);
            exit_code = 1;
        } else {
            println!("✓ {} is valid", &file_path)
        };
    }
    process::exit(exit_code);
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_direct_map_field() {
        #[derive(Deserialize)]
        struct Model {
            field: DirectMapField,
        }
        let input = r#"
            field: value
        "#;
        let maybe_yaml: Result<Model, serde_yaml::Error> = serde_yaml::from_str(input);
        assert!(maybe_yaml.is_ok());
    }

    #[test]
    fn test_constant_field() {
        #[derive(Deserialize)]
        struct Model {
            field: ConstantField,
        }
        let input = r#"
            field: 
                const: value
        "#;
        let maybe_yaml: Result<Model, serde_yaml::Error> = serde_yaml::from_str(input);
        assert!(maybe_yaml.is_ok());
    }

    #[test]
    fn test_prefix_field() {
        #[derive(Deserialize)]
        struct Model {
            field: PrefixField,
        }
        let input = r#"
            field: 
                prefix:
                    value: prefix_value
                    field: link
        "#;
        let maybe_yaml: Result<Model, serde_yaml::Error> = serde_yaml::from_str(input);
        assert!(maybe_yaml.is_ok());
    }

    #[test]
    fn test_list_field() {
        #[derive(Deserialize)]
        struct Model {
            list_field: ListField,
        }
        let input = r#"
            list_field:
                - link
                - const:
                    const_value
                - prefix:
                    value: prefix_value
                    field: link
        "#;
        let maybe_yaml: Result<Model, serde_yaml::Error> = serde_yaml::from_str(input);
        assert!(maybe_yaml.is_ok());
    }

    #[test]
    fn test_language_field() {
        #[derive(Deserialize)]
        struct Model {
            lang_field: LanguageField,
        }
        let input = r#"
            lang_field:
                de: a_german_title
                fr:
                    - a_french_title
                    - const: contenu
                    - prefix:
                        value: prefixe
                        field: link
        "#;
        let maybe_yaml: Result<Model, serde_yaml::Error> = serde_yaml::from_str(input);
        assert!(maybe_yaml.is_ok());
    }
    #[test]
    fn test_any_field() {
        #[derive(Deserialize)]
        struct Model {
            any_value_field: Any,
        }
        let input = r#"
            any_value_field:
                de: value
                fr:
                    - a_french_title
                    - const: contenu
                    - prefix:
                        value: prefixe
                        field: link
                it:
                    - list_elem_1
                    - list_elem_2
        "#;
        let maybe_yaml: Result<Model, serde_yaml::Error> = serde_yaml::from_str(input);
        assert!(maybe_yaml.is_ok());
    }
}
