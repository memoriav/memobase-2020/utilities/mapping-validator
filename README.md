# Mapping Validator

Utility tool to validate Memobase data model mappers against the expected schema.

## Usage

```sh
mapping-validator <path_to_first_mapping_file.yaml> <path_to_second_mapping_file.yaml> <...>
```
